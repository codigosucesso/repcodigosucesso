# Projeto Portal Crejoá #

Este é o repositório do Portal Crejoá.

### Equipe ###

* Ana Luiza Martins Cesário

* Arthur Rezende de Faria
* Github: https://github.com/arthurezende

* Diego Augusto Ramos

* Rafael Dalonso Schultz

* Rafael Silva de Oliveira

* Hieda Nicodemo de Oliveira

* Julia Cardoso da Silva e Almeida

### Professor ###

* Straus Michalsky Martins

### Instituição de Ensino ###

* Instituto Federal de Educação, Ciência e Tecnologia do Sul de Minas Gerais Campus Poços de Caldas