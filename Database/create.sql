DROP DATABASE IF EXISTS projeto;
CREATE database projeto;
USE projeto;
create table Usuario(
cod int not null auto_increment,
nome varchar(50),
login varchar(20),
senha varchar(20),
permissoes int,
email varchar(50),
primary key(cod)
);
create table PontoColeta(
idPonto int not null auto_increment,
dataAdicao varchar(10),
descricao varchar(200),
endereco varchar(100),
latitude double,
longitude double,
telefone varchar(20),
dataVerificacao date,
primary key(idPonto)
);
create table Artigo(
dataPublicacao varchar(10),
dataAprovacao varchar(10),
idArtigo int not null auto_increment,
titulo varchar(30),
primary key(idArtigo)
);
