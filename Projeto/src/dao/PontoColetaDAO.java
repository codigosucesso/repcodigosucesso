package dao;

import java.sql.Connection;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import com.teamdev.jxmaps.LatLng;
import model.ModeloMySql;
import model.PontoColeta;
import controller.FabricaDeConexao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PontoColetaDAO {
	private ModeloMySql modelo = null;

	public PontoColetaDAO() {
		modelo = new ModeloMySql();
		modelo.setDatabase("projeto");
		modelo.setServer("localhost");
		modelo.setUser("root");
		modelo.setPassword("ifsuldeminas");
		modelo.setPort("3306");
	}

	/**M�todo para cadastrar no banco um Ponto de Coleta
	 * @param location LatLng - localiza��o do Ponto de Coleta
	 * @param endereco String - Endere�o do Ponto de Coleta
	 * @param descricao String - Descri��o do Ponto de Coleta
	 * @param telefone String - Telefone do Ponto de Coleta
	 * @return boolean - true caso o ponto de coleta seja cadastrado com sucesso e false caso contr�rio*/
	public boolean Geocodification(LatLng location, String endereco, String descricao, String telefone) {

		Connection con;
		con = FabricaDeConexao.getConnection(modelo);

		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conex�o ao banco");
		}

		String sql = "INSERT into pontocoleta(latitude,longitude,descricao,endereco,telefone) values(?,?,?,?,?)";
		/*
		 * String sql =
		 * "UPDATE PontoColeta set latitude = ? ,longitude = ? from PontoColeta where endereco = ?"
		 * ;
		 */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			statement.setDouble(1, location.getLat());
			statement.setDouble(2, location.getLng());
			statement.setString(3, telefone);
			statement.setString(4, endereco);
			statement.setString(5, descricao);
			statement.executeUpdate();
			// NOTA: Por algum motivo dos infernos, ele t� trocando a descricao com o
			// telefone, ent�o eu inverti
			// na hora de botar as interroga��es e ficou 100% funcional

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FabricaDeConexao.closeConnection(con);
			return false;
		}

		return true;
	}

	/**M�todo para retornar do banco todos os pontos de coleta cadastrados
	 * @return ArrayList<PontoColeta>- true caso a lista de pontos de coleta seja consultada com sucesso e null caso contr�rio*/
	public ArrayList<PontoColeta> pontosSelect() {

		Connection con;
		con = FabricaDeConexao.getConnection(modelo);

		// testa se conseguiu conectar
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conex�o ao banco");
		}

		ArrayList<PontoColeta> pontos = new ArrayList<>();

		String sql = "SELECT * from pontocoleta";
		PontoColeta p;

		try {
			PreparedStatement statement = con.prepareStatement(sql);
			ResultSet result = statement.executeQuery();

			while (result.next()) {
				p = new PontoColeta();
				LatLng c = new LatLng(result.getDouble("latitude"), result.getDouble("longitude"));
				p.setCoordenadas(c);
				p.setIdPonto(result.getInt("idPonto"));
				p.setDescricao(result.getString("descricao"));
				p.setEndereco(result.getString("endereco"));
				p.setTelefone(result.getString("telefone"));
				pontos.add(p);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}
		return pontos;
	}
}
