package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;
import model.Usuario;

public class UsuarioDAO {
	private ModeloMySql modelo = null;

	public UsuarioDAO() {

		Autenticacao au = new Autenticacao();
		modelo = new ModeloMySql();
		modelo = au.definirModelo();

	}

	/**M�todo para cadastrar no banco um usu�rio Padr�o
	 * @param pessoa Usuario - objeto Usuario que ser� inserido no Banco de Dados
	 * @return boolean - true caso o usu�rio seja inserido com sucesso e false caso contr�rio*/
	public boolean Inserir(Usuario pessoa) {

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conexao ao banco");
			return false;
		}

		/* PREPARA O SQL PARA INSERIR NO BANCO */

		String sql = "INSERT INTO usuario (nome, email, login, senha) " + "VALUES(?,?,?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, pessoa.getNome());
			statement.setString(2, pessoa.getEmail());
			statement.setString(3, pessoa.getLogin());
			statement.setString(4, pessoa.getSenha());
			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "ta entrando na exce��o");
			status = false;
		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}

		return status;
	}

	/**M�todo para pesquisar no banco um usu�rio Padr�o
	 * @param email String - e-mail que ser� utilizado na busca ao Banco de Dados
	 * @return Usuario - usu�rio Padr�o encontrado na busca ao Banco de Dados ou null caso nada seja encontrado*/
	public Usuario Pesquisar(String email) {

		Connection con = FabricaDeConexao.getConnection(modelo);

		Usuario p = new Usuario();

		/* testa se conseguiu conectar */
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conexao ao banco");
			return null;
		}

		/* prepara o sql para inserir no banco */
		String sql = "SELECT * FROM usuario WHERE email='" + email + "';";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			ResultSet result = statement.executeQuery();

			if (result.next()) {
				/* preenche com os dados */

				p.setNome(result.getString("nome"));
				p.setEmail(result.getString("email"));
				p.setLogin(result.getString("login"));
				p.setSenha(result.getString("senha"));
			} else {
				return null;
			}
			/* executa o comando no banco */
			// statement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}
		return p;
	}
	
	/**M�todo para realizar login de um usu�rio Moderador
	 * @param login String - Login que ser� utilizado para realizar o processo de Login
	 * @return Usuario - usu�rio que ser� comparado para que o Login seja realizado*/
	public Usuario Login(String login) {

		Connection con = FabricaDeConexao.getConnection(modelo);

		Usuario p = new Usuario();

		/* testa se conseguiu conectar */
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conexao ao banco");
			return null;
		}

		/* prepara o sql para inserir no banco */
		String sql = "SELECT * FROM usuario WHERE login='" + login + "';";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			ResultSet result = statement.executeQuery();

			if (result.next()) {
				/* preenche com os dados */

				p.setNome(result.getString("nome"));
				p.setEmail(result.getString("email"));
				p.setLogin(result.getString("login"));
				p.setSenha(result.getString("senha"));
			} else {
				return null;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}
		return p;
	}

	/**M�todo para alterar a senha no banco de um usu�rio Padr�o
	 * @param login String - Login que ser� alterado a senha
	 * @param senha String - nova senha que ser� inserida no banco
	 * @return boolean - true caso a senha seja alterada com sucesso e false caso contr�rio*/
	public boolean AlterarSenha(String login, String senha) {

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conexao ao banco");
			return false;
		}

		/* PREPARA O SQL PARA INSERIR NO BANCO */

		/* prepara para inserir no banco de dados */

		try {
			String query = "update usuario set senha = ? where login = ?";

			PreparedStatement preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, senha);
			preparedStmt.setString(2, login);
			preparedStmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}

		return status;
	}

	/**M�todo para alterar os dados cadastrais no banco de um usu�rio Padr�o
	 * @param login String - Login que ser� alterado
	 * @param nome String - Nome que ser� alterado
	 * @param email String - e-mail que ser� alterado
	 * @return boolean - true caso os dados cadastrais sejam alterados com sucesso e false caso contr�rio*/
	public boolean AlterarDados(String login, String nome, String email) {

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conexao ao banco");
			return false;
		}

		/* prepara para inserir no banco de dados */

		try {
			String query = "update usuario set nome = ? , email = ? where login = ?";

			PreparedStatement preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, nome);
			preparedStmt.setString(2, email);
			preparedStmt.setString(3, login);
			preparedStmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}

		return status;
	}

	/**M�todo para deletar no banco de um usu�rio Padr�o
	 * @param login String - Login que do usu�rio que ser� deletado
	 * @return boolean - true caso o usu�rio seja deletado com sucesso e false caso contr�rio*/
	public boolean DeletaUsuario(String login) {
		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conexao ao banco");
			return false;
		}

		/* prepara para inserir no banco de dados */

		try {
			String query = "Delete from usuario where login = ?";

			PreparedStatement preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, login);
			preparedStmt.executeUpdate();
			;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}

		return status;
	}
}
