package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;
import model.Editor;

public class EditorDAO {
	private ModeloMySql modelo = null;

	public EditorDAO() {
		Autenticacao au = new Autenticacao();
		modelo = new ModeloMySql();
		modelo = au.definirModelo();
	}

	/**M�todo para cadastrar no banco um usu�rio Editor
	 * @param pessoa Editor - objeto Editor que ser� inserido no Banco de Dados
	 * @return boolean - true caso o usu�rio seja inserido com sucesso e false caso contr�rio*/
	public boolean Inserir(Editor pessoa) {

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conex���o ao banco");
			return false;
		}

		/* PREPARA O SQL PARA INSERIR NO BANCO */

		String sql = "INSERT INTO usuario (nome, email, login, senha, permissoes) " + "VALUES(?,?,?,?,?);";

		/*
		 * Modificar tabela pessos para usuario/
		 * 
		 * /* prepara para inserir no banco de dados
		 */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, pessoa.getNome());
			statement.setString(2, pessoa.getEmail());
			statement.setString(3, pessoa.getLogin());
			statement.setString(4, pessoa.getSenha());
			// AQUI VAI A PERMISS�O DE CADA USUARIO
			statement.setInt(5, 3);
			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}

		return status;
	}

	/**M�todo para pesquisar no banco um usu�rio Editor
	 * @param email String - e-mail que ser� utilizado na pesquisa ao Banco de Dados
	 * @return Editor - usu�rio editor retornado na busca ou null caso nenhum usu�rio seja encontrado*/
	public Editor Pesquisar(String email) {

		Connection con = FabricaDeConexao.getConnection(modelo);

		Editor p = new Editor();

		/* testa se conseguiu conectar */
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conexao ao banco");
			return null;
		}

		/* prepara o sql para inserir no banco */
		String sql = "SELECT * FROM usuario WHERE email=" + email + ";";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			ResultSet result = statement.executeQuery();

			if (result.next()) {
				/* preenche com os dados */

				p.setNome(result.getString("nome"));
				p.setEmail(result.getString("email"));
				p.setLogin(result.getString("login"));
				p.setSenha(result.getString("senha"));
			} else {
				return null;
			}
			/* executa o comando no banco */
			// statement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}
		return p;
	}
}
