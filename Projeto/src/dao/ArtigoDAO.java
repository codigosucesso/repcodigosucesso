package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;
import model.Artigo;
import dao.Autenticacao;

public class ArtigoDAO {
	private ModeloMySql modelo = null;

	public ArtigoDAO() {
		Autenticacao au = new Autenticacao();
		modelo = new ModeloMySql();
		modelo = au.definirModelo();
	}

	/**M�todo para cadastrar no banco um Artigo
	 * @param artigo Artigo - objeto Artigo que ser� inserido no Banco de Dados
	 * @return boolean - true caso o artigo seja inserido com sucesso e false caso contr�rio*/
	public boolean Inserir(Artigo artigo) {

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conex�o ao banco");
			return false;
		}

		/* prepara o sql para inserir no banco */

		String sql = "INSERT INTO artigo (dataPublicacao, dataAprovacao, titulo) " + "VALUES(?,?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			/* preenche com os dados */
			statement.setString(1, artigo.getDataPublicacao());
			statement.setString(2, artigo.getDataAprovacao());
			statement.setString(3, artigo.getTitulo());

			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}

		return status;
	}

	/**M�todo para pesquisar no banco um Artigo
	 * @param idArtigo int - id do Artigo que ser� pesquisado no Banco de Dados
	 * @return Artigo - artigo retornado na busca ou null caso nada seja encontrado*/
	public Artigo Pesquisar(int idArtigo) {
		Connection con = FabricaDeConexao.getConnection(modelo);

		Artigo a = new Artigo();

		/* testa se conseguiu conectar */
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conexao ao banco");
			return null;
		}

		/* prepara o sql para inserir no banco */
		String sql = "SELECT * FROM artigo WHERE idArtigo=" + idArtigo + ";";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			ResultSet result = statement.executeQuery();

			if (result.next()) {
				/* preenche com os dados */

				a.setDataPublicacao(result.getString("dataPublicao"));
				a.setDataAprovacao(result.getString("dataAprovacao"));
				a.setIdArtigo(result.getInt("idArtigo"));
				a.setTitulo(result.getString("titulo"));
			} else {
				return null;
			}
			/* executa o comando no banco */
			// statement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}
		return a;
	}
}
