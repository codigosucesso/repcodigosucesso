package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import controller.FabricaDeConexao;
import model.ModeloMySql;
import model.Administrador;


public class AdministradorDAO {
	
	private ModeloMySql modelo = null;
	
	public AdministradorDAO() {
		Autenticacao au = new Autenticacao();
		modelo = new ModeloMySql();
		modelo = au.definirModelo();
	}
	
	/**M�todo para cadastrar no banco um usu�rio Administrador
	 * @param pessoa Administrador - objeto Administrador que ser� inserido no Banco de Dados
	 * @return boolean - true caso o usu�rio seja inserido com sucesso e false caso contr�rio*/
	public boolean Inserir(Administrador pessoa) {

		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conex���o ao banco");
			return false;
		}

		/* PREPARA O SQL PARA INSERIR NO BANCO */

		String sql = "INSERT INTO usuario (nome, email, login, senha, permissoes) " + "VALUES(?,?,?,?,?);";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);
			int pe = 1;
			/* preenche com os dados */
			statement.setString(1, pessoa.getNome());
			statement.setString(2, pessoa.getEmail());
			statement.setString(3, pessoa.getLogin());
			statement.setString(4, pessoa.getSenha());
			// AQUI VAI A PERMISS�O DE CADA USUARIO
			statement.setInt(5, pe);
			/* executa o comando no banco */
			statement.executeUpdate();
			status = true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}

		return status;
	}

	/**M�todo para pesquisar no banco um usu�rio Administrador
	 * @param email String - e-mail do usu�rio Administrador
	 * @return Administrador - usu�rio retornado na pesquisa ao Banco de Dados*/
	public Administrador Pesquisar(String email) {
		Connection con = FabricaDeConexao.getConnection(modelo);

		Administrador p = new Administrador();

		/* testa se conseguiu conectar */
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conexao ao banco");
			return null;
		}

		/* prepara o sql para inserir no banco */
		String sql = "SELECT * FROM usuario WHERE email=" + email + ";";

		/* prepara para inserir no banco de dados */

		try {
			PreparedStatement statement = con.prepareStatement(sql);

			ResultSet result = statement.executeQuery();

			if (result.next()) {
				/* preenche com os dados */

				p.setNome(result.getString("nome"));
				p.setEmail(result.getString("email"));
				p.setLogin(result.getString("login"));
				p.setSenha(result.getString("senha"));
			} else {
				return null;
			}
			/* executa o comando no banco */
			// statement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}
		return p;
	}

	/**M�todo para deletar do banco um usu�rio Administrador
	 * @param login String - login do Administrador que ser� deletado do Banco de Dados
	 * @return boolean - true caso o usu�rio seja deletado com sucesso e false caso contr�rio*/
	public boolean DeletaAdministrador(String login) {
		boolean status = true;
		Connection con = FabricaDeConexao.getConnection(modelo);

		/* testa se conseguiu conectar */
		if (con == null) {
			JOptionPane.showMessageDialog(null, "Erro de conexao ao banco");
			return false;
		}

		/* prepara para inserir no banco de dados */

		try {
			String query = "Delete from usuario where login = '" + login + "'";

			PreparedStatement preparedStmt = con.prepareStatement(query);
			System.out.println(query);
			preparedStmt.executeUpdate();
			;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		} finally {
			// Fecha a conexao
			FabricaDeConexao.closeConnection(con);
		}

		return status;
	}
}
