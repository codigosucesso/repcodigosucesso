package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import model.ModeloMySql;

public class FabricaDeConexao {
	
	/**M�todo para criar conex�es com o Banco de Dados
	 * @param dados ModeloMySql - modelo do Banco de Dados que ser� seguido para cria��o das conex�es
	 * @return Connection - retorna uma conex�o com o banco de dados*/
	public static Connection getConnection(ModeloMySql dados) {

		// padr�o de conex�o do mysql
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			return DriverManager.getConnection(dados.toString(), dados.getUser(), dados.getPassword());
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**M�todo para fechar as conex�es
	 * @param c Connection - Conex�o que ser� fechada*/
	public static void closeConnection(Connection c) {

		try {
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
