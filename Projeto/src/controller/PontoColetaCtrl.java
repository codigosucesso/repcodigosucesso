package controller;

import javax.swing.JOptionPane;

import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.GeocoderCallback;
import com.teamdev.jxmaps.GeocoderRequest;
import com.teamdev.jxmaps.GeocoderResult;
import com.teamdev.jxmaps.GeocoderStatus;
import com.teamdev.jxmaps.InfoWindow;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Marker;
import com.teamdev.jxmaps.swing.MapView;

import dao.PontoColetaDAO;

public class PontoColetaCtrl extends MapView {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	MapView mapView = new MapView();
	public double latitude;
	public double longitude;

	/**Construtor padr�o da classe PontoColetaCtrl*/
	public PontoColetaCtrl() {}

	/**M�todo para geocodificar endere�os. 
	 * Ap�s geocodifica��o, passa como param�tro para a fun��o "Geocodification" as informa��es do Ponto de Coleta para inser��o no Banco de Dados
	 * @param endereco String - endere�o do ponto de coleta
	 * @param telefone String - telefone do ponto de coleta
	 * @param descricao String - Descri��o do ponto de coleta*/
	public void performGeocode(String endereco, String telefone, String descricao) {
		setOnMapReadyHandler(new MapReadyHandler() {
			@Override
			public void onMapReady(MapStatus status) {
				// Getting the associated map object
				final Map map = getMap();
				// Creating a geocode request
				GeocoderRequest request = new GeocoderRequest();
				// Setting address to the geocode request
				request.setAddress(endereco);

				getServices().getGeocoder().geocode(request, new GeocoderCallback(map) {
					@Override
					public void onComplete(GeocoderResult[] results, GeocoderStatus status) {
						// Checking operation status
						boolean r = false;
						if ((status == GeocoderStatus.OK) && (results.length > 0)) {
							// Getting the first result
							GeocoderResult result = results[0];
							// Getting a location of the result
							LatLng location = result.getGeometry().getLocation();

							latitude = location.getLat();
							longitude = location.getLng();

							PontoColetaDAO p = new PontoColetaDAO();
							r = p.Geocodification(location, endereco, telefone, descricao);
						}
					}
				});
			}
		});
	}

}
