package controller;

import javax.swing.JOptionPane;

import dao.AdministradorDAO;
import model.Administrador;

public class AdministradorCtrl {

	/**M�todo para cadastrar Administrador
	 * @param nome String- nome do usu�rio
	 * @param email String - e-mail do usu�rio
	 * @param login String- login do usu�rio
	 * @param senha String - senha do usu�rio
	 * @return boolean - resposta para a afirma��o: "O usu�rio foi cadastrado."*/
	public boolean Cadastrar(String nome, String email, String  login, String senha){
		
		Administrador p = new Administrador();
		
		p.setNome(nome);
		p.setEmail(email);
		p.setLogin(login);
		p.setSenha(senha);
		AdministradorDAO dao = new AdministradorDAO();
		
		if(dao.Inserir(p)){
			JOptionPane.showMessageDialog(null, "Dados inseridos no banco!");
			return true;
		} else {
			JOptionPane.showMessageDialog(null, "Erro ao gravar no banco!");
			return false;
		}
	}
	
	/**M�todo para pesquisar Usu�rios
	 * @param email String - e-mail do usu�rio
	 * @return Administrador - usu�rio encontrado ou null caso nenhum usu�rio seja encontrado.*/
	public Administrador Pesquisar(String email){
		
		AdministradorDAO dao = new AdministradorDAO();
		Administrador p = dao.Pesquisar(email);
		if(p != null){
			/*JOptionPane.showMessageDialog(null, "Encontrado com suecesso:"
					+ "\nEmail"+p.getEmail()+"\nNome:"+p.getNome()+"/nLogin:"+p.getLogin());*/
			return p;
		} else {
			JOptionPane.showMessageDialog(null, "Erro: essa pessoa nao existe!");
		}
		return null;
	}

}
