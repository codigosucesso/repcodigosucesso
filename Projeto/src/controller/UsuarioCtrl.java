package controller;

import javax.swing.JOptionPane;

import dao.UsuarioDAO;
import model.Usuario;

public class UsuarioCtrl {
	
	/**M�todo para cadastrar usu�rios
	 * @param nome String- nome do usu�rio
	 * @param email String - e-mail do usu�rio
	 * @param login String- login do usu�rio
	 * @param senha String - senha do usu�rio
	 * @return boolean - Retorna true se o cadastro for bem sucedido e false se o contr�rio ocorrer*/
	public boolean Cadastrar(String nome, String email, String  login, String senha){
		
		Usuario p = new Usuario(nome,login, email, senha);
		UsuarioDAO dao = new UsuarioDAO();
		
		if(dao.Inserir(p)){
			JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
			return true;
		} else {
			JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
			return false;
		}
	}
	
	/**M�todo para pesquisar Usu�rios
	 * @param email String - e-mail do usu�rio
	 * @return Usuario - retorna o usu�rio resultante da pesquisa ou null caso este n�o seja encontrado*/
	public Usuario Pesquisar(String email){
		
		UsuarioDAO dao = new UsuarioDAO();
		Usuario p = dao.Pesquisar(email);
		if(p != null){
			/*JOptionPane.showMessageDialog(null, "Encontrado com suecesso:"
					+ "\nEmail"+p.getEmail()+"\nEmail:"+p.getEmail()+"/nLogin:"+p.getLogin());*/
			return p;
		} else {
			JOptionPane.showMessageDialog(null, "Erro: essa pessoa nao existe!");
			return null;
		}
	}

	/**M�todo para realizar Login de usu�rios
	 * @param login String- login do usu�rio
	 * @param senha String - senha do usu�rio
	 * @return int - Retorna 1 se os dados forem alterados; 2 se o �suario errou a senha e/ou login; e 0 se o usu�rio n�o existir no banco de Dados*/
	public int Login(String login, String senha){
		UsuarioDAO dao = new UsuarioDAO();
		Usuario p = dao.Login(login);
		if(p != null){
			if(p.getSenha().equals(senha)){
				//logou
				return 1;
			}
			//errou
			return 2;
			
		} else {
			//nao existe
			return 0;
		}
		
	}
	
	/**M�todo para alterar senha de usu�rios
	 * @param login String- login do usu�rio
	 * @param senha String - senha atual do usu�rio
	 * @param novasenha String - nova senha do us�ario
	 * @return int - Retorna 1 se os dados forem alterados; 2 se o �suario errou a senha e/ou login; e 0 se o usu�rio n�o existir no banco de Dados*/
	public int AlterarSenha(String login, String senha, String novasenha){
		UsuarioDAO dao = new UsuarioDAO();
		Usuario p = dao.Login(login);
		if(p != null){
			if(p.getSenha().equals(senha)){
				boolean f = dao.AlterarSenha(login,novasenha);
				if(f){
					JOptionPane.showMessageDialog(null,"Alterou senha");
					return 1;
				}
			}
			//errou
			return 2;
		} else {
			//nao exist
			JOptionPane.showMessageDialog(null,"N�o alterou a senha.");
			return 0;
		}
		
	}
	
	/**M�todo para alterar os dados cadastrais dos usu�rios
	 * @param nome String- novo nome do usu�rio
	 * @param email String - novo e-mail do usu�rio
	 * @param login String- novo login do usu�rio
	 * @param senha String - nova senha do usu�rio
	 * @return int - Retorna 1 se os dados forem alterados; 2 se o �suario errou a senha e/ou login; e 0 se o usu�rio n�o existir no banco de Dados*/
	public int AlterarDados(String login, String senha, String nome, String email){
		UsuarioDAO dao = new UsuarioDAO();
		Usuario p = dao.Login(login);
		if(p != null){
			if(p.getSenha().equals(senha)){
				boolean f = dao.AlterarDados(login, nome, email);
				if(f){
					return 1;
				}else{
					return 0;
				}
			}
			//errou
			return 2;
		} else {
			//nao exist
			return 0;
		}
		
	}
	
	/**M�todo para deletar Usu�rios
	 * @param login String- login do usu�rio*/
	public boolean DeletarUsuario(String login) {
		UsuarioDAO dao = new UsuarioDAO();
		dao.DeletaUsuario(login);
		return false;
	}
}
