package controller;

import javax.swing.JOptionPane;

import dao.EditorDAO;
import model.Editor;

public class EditorCtrl {
	
	/**M�todo para cadastrar editores
	 * @param nome String- nome do usu�rio
	 * @param email String - e-mail do usu�rio
	 * @param login String- login do usu�rio
	 * @param senha String - senha do usu�rio*/
	public void Cadastrar(String nome, String email, String login, String senha) {

		Editor p = new Editor();

		p.setNome(nome);
		p.setEmail(email);
		p.setLogin(login);
		p.setSenha(senha);

		EditorDAO dao = new EditorDAO();

		if (dao.Inserir(p)) {

			JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
		} else {
			JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
		}

	}
	
	/**M�todo para pesquisar Editores
	 * @param email String - e-mail do editor*/
	public void Pesquisar(String email) {

		EditorDAO dao = new EditorDAO();
		Editor p = dao.Pesquisar(email);
		if (p != null) {
			JOptionPane.showMessageDialog(null, "Encontrado com suecesso:" + "\nEmail:" + p.getEmail() + "\nNome:"
					+ p.getNome() + "/nLogin:" + p.getLogin());
		} else {
			JOptionPane.showMessageDialog(null, "Erro: essa pessoa nao existe!");
		}

	}

}
