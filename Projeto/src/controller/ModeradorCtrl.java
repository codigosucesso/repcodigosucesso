package controller;

import javax.swing.JOptionPane;

import model.Moderador;
import dao.ModeradorDAO;

public class ModeradorCtrl {
	
	/**M�todo para cadastrar Moderadores
	 * @param nome String- nome do usu�rio
	 * @param email String - e-mail do usu�rio
	 * @param login String- login do usu�rio
	 * @param senha String - senha do usu�rio*/
	public void Cadastrar(String nome, String email, String login, String senha) {

		Moderador p = new Moderador();

		p.setNome(nome);
		p.setEmail(email);
		p.setLogin(login);
		p.setSenha(senha);

		ModeradorDAO dao = new ModeradorDAO();

		if (dao.Inserir(p)) {

			JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
		} else {
			JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
		}

	}

	/**M�todo para pesquisar moderadores
	 * @param email String - e-mail do moderador*/
	public void Pesquisar(String email) {

		ModeradorDAO dao = new ModeradorDAO();
		Moderador p = dao.Pesquisar(email);
		if (p != null) {
			JOptionPane.showMessageDialog(null, "Encontrado com suecesso:" + "\nEmail" + p.getEmail() + "\nNome:"
					+ p.getNome() + "/nLogin:" + p.getLogin());
		} else {
			JOptionPane.showMessageDialog(null, "Erro: essa pessoa nao existe!");
		}

	}

}
