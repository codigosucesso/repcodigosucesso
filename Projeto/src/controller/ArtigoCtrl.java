package controller;

import javax.swing.JOptionPane;

import dao.ArtigoDAO;
import model.Artigo;

public class ArtigoCtrl {
	
	/**M�todo para cadastrar informa��es de Artigos
	 * @param idArtigo int - id do Artigo
	 * @param dataPublicacao String - Data de publica��o do Artigo
	 * @param dataAprovacao String- Data de aprova��o para publica��o do Artigo
	 * @param titulo String - T�tulo dos artigo*/
	public void Cadastrar(int idArtigo, String dataPublicacao, String dataAprovacao, String titulo){
		
		Artigo a = new Artigo();
		
		a.setIdArtigo(idArtigo);
		a.setDataPublicacao(dataPublicacao);
		a.setDataAprovacao(dataAprovacao);
		a.setTitulo(titulo);
		
		ArtigoDAO dao = new ArtigoDAO();
		
		if(dao.Inserir(a)){

			JOptionPane.showMessageDialog(null, "Dados inseridos no banco !");
		} else {
			JOptionPane.showMessageDialog(null, "Erro ao gravar no banco !");
		}

	}
	
	/**M�todo para pesquisar artigos
	 * @param idArtigo int - identifica��o do artigo*/
	public void Pesquisar(int idArtigo){
		
		ArtigoDAO dao = new ArtigoDAO();
		Artigo a = dao.Pesquisar(idArtigo);
		if(a != null){
			JOptionPane.showMessageDialog(null, "Encontrado com sucesso:"
					+ "\nID:"+a.getIdArtigo()+"\nTitulo:"+a.getTitulo());
		} else {
			JOptionPane.showMessageDialog(null, "Erro: essa pessoa nao existe!");
		}
		
	}
}
