package view;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.InfoWindow;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.MapViewOptions;
import com.teamdev.jxmaps.Marker;
import com.teamdev.jxmaps.swing.MapView;
import com.teamdev.jxmaps.Map;

import dao.PontoColetaDAO;

import model.PontoColeta;

import controller.PontoColetaCtrl;

public class Mapa extends MapView {
	
	
	private static ArrayList<PontoColeta> pontos;
	JFrame frame = new JFrame("Crejoa | Mapa com os Pontos de Coleta");
	
	
	public Mapa(MapViewOptions options) {
        super(options);
        
        PontoColetaDAO tm=new PontoColetaDAO();
		
		pontos=tm.pontosSelect();
		
		//MapViewOptions options = new MapViewOptions();
        options.importPlaces();
        //final Mapa mapView = new Mapa(options);

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(this, BorderLayout.CENTER);
        frame.setSize(700, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        setOnMapReadyHandler(new MapReadyHandler() {
            @Override
            public void onMapReady(MapStatus status) {
                if (status == MapStatus.MAP_STATUS_OK) {
                    final Map map = getMap();
                    map.setZoom(10.0);
                    MapOptions options = new MapOptions();
                    // Creating a map type control options object
                    MapTypeControlOptions controlOptions = new MapTypeControlOptions();
                    // Changing position of the map type control
                    controlOptions.setPosition(ControlPosition.TOP_RIGHT);
                    // Setting map type control options
                    options.setMapTypeControlOptions(controlOptions);
                    // Setting map options
                    map.setOptions(options);
                    // Setting the map center 
                    map.setCenter(new LatLng(-21.83704,-46.559189));
                    
                    
                    ArrayList<Marker> markers=new ArrayList<>();
                    ArrayList<InfoWindow> infos=new ArrayList<>();
                    
                    for(int i=0;i<pontos.size();i++){
                    	Marker m=new Marker(map);
                    	m.setPosition(pontos.get(i).getCoordenadas());
                    	InfoWindow a=new InfoWindow(map);
                    	a.setContent(pontos.get(i).getDescricao());
                    	a.setPosition(pontos.get(i).getCoordenadas());
                    	markers.add(m);
                    	infos.add(a);
                    }
                    for(int i=0;i<pontos.size();i++){
                    infos.get(i).open(map, markers.get(i));
                    }
                    
                    
                }
            }
        });

	}
}
