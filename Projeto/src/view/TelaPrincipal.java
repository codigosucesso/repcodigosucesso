package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.teamdev.jxmaps.MapViewOptions;

import controller.AdministradorCtrl;
import controller.EditorCtrl;
import controller.ModeradorCtrl;
import controller.PontoColetaCtrl;
import controller.UsuarioCtrl;

import view.Mapa;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.CardLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import javax.swing.JFormattedTextField;
import javax.swing.DropMode;
import javax.swing.AbstractAction;
import javax.swing.Action;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.JTextPane;

public class TelaPrincipal extends JFrame {

	private JPanel contentPane;
	private JTextField textTelefonePontoColeta;
	private JTextField textEnderecoPontoColeta;
	private JTextField textDescricaoPontoColeta;
	private JTextField textNomeAdministrador;
	private JTextField textCPFAdministrador;
	private JTextField textEmailAdministrador;
	private JTextField textDataNascAdministrador;
	private JTextField textLoginAdministrador;
	private JPasswordField passwordAdministrador;
	private JTextField textNomeEditor;
	private JTextField textCPFEditor;
	private JTextField textEmailEditor;
	private JTextField textDataNascEditor;
	private JTextField textLoginEditor;
	private JPasswordField passwordEditor;
	private JTextField textNomeModerador;
	private JTextField textCPFModerador;
	private JTextField textEmailModerador;
	private JTextField textDataNascModerador;
	private JTextField textLoginModerador;
	private JPasswordField passwordModerador;
	private JTextField txtLoginAlterar;
	private JTextField txtSenhaAntigaAlterar;
	private JTextField txtSenhaNovaAlterar;
	private JTextField txtLoginAltDados;
	private JTextField txtSenhaAltDados;
	private JTextField txtNomeAlterar;
	private JTextField txtEmailAlterar;
	private JTextField txtLoginDel;
	private JPasswordField txtSenhaDel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal frame = new TelaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaPrincipal() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(TelaPrincipal.class.getResource("/img/icons8-pointer.png")));
		setTitle("Crejo\u00E1 | In\u00EDcio");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 690, 423);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnInicio = new JMenu("Inicio");
		
		menuBar.add(mnInicio);
		
		JMenu mnPessoas = new JMenu("Usu\u00E1rios");
	
		menuBar.add(mnPessoas);
		
		JMenuItem mntmAlterar = new JMenuItem("Alterar");
		
		mnPessoas.add(mntmAlterar);
		
		JMenuItem mntmExcluir = new JMenuItem("Excluir");
		
		mnPessoas.add(mntmExcluir);
		
		JMenu mnArtigos = new JMenu("Artigos");
		
		menuBar.add(mnArtigos);
		
		JMenuItem mntmEmConstruo = new JMenuItem("EM CONSTRU\u00C7\u00C3O");
		mnArtigos.add(mntmEmConstruo);
		
		JMenu mnMapa = new JMenu("Mapa");
		
		menuBar.add(mnMapa);
		
		JMenu mnContatos = new JMenu("Contatos");
		menuBar.add(mnContatos);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 690, 379);
		contentPane.add(panel);
		panel.setLayout(new CardLayout(0, 0));
		
		JPanel inicio = new JPanel();
		panel.add(inicio, "name_166070114857929");
		inicio.setLayout(null);
		
		JLabel label_6 = new JLabel("");
		label_6.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/bem vindo menu.png")));
		label_6.setBounds(0, 26, 680, 368);
		inicio.add(label_6);
		
		JLabel label_2 = new JLabel("");
		label_2.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/fundo crejoa.png")));
		label_2.setBounds(0, 0, 680, 394);
		inicio.add(label_2);
		
		JPanel cadastroPontoColeta = new JPanel();
		panel.add(cadastroPontoColeta, "name_840788379422");
		cadastroPontoColeta.setLayout(null);
		
		textTelefonePontoColeta = new JTextField();
		textTelefonePontoColeta.setColumns(10);
		textTelefonePontoColeta.setBounds(132, 193, 209, 26);
		cadastroPontoColeta.add(textTelefonePontoColeta);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setForeground(Color.WHITE);
		lblTelefone.setBounds(59, 198, 61, 16);
		cadastroPontoColeta.add(lblTelefone);
		
		JLabel lblEndereo = new JLabel("Endere\u00E7o:");
		lblEndereo.setForeground(Color.WHITE);
		lblEndereo.setBounds(59, 144, 61, 16);
		cadastroPontoColeta.add(lblEndereo);
		
		textEnderecoPontoColeta = new JTextField();
		textEnderecoPontoColeta.setColumns(10);
		textEnderecoPontoColeta.setBounds(132, 144, 416, 26);
		cadastroPontoColeta.add(textEnderecoPontoColeta);
		
		textDescricaoPontoColeta = new JTextField();
		textDescricaoPontoColeta.setColumns(10);
		textDescricaoPontoColeta.setBounds(132, 97, 416, 26);
		cadastroPontoColeta.add(textDescricaoPontoColeta);
		
		JLabel lblDescrio = new JLabel("Descri\u00E7\u00E3o:");
		lblDescrio.setForeground(Color.WHITE);
		lblDescrio.setBounds(59, 102, 61, 16);
		cadastroPontoColeta.add(lblDescrio);
		
		JLabel lblCadastroDoPonto = new JLabel("Cadastro do Ponto de Coleta");
		lblCadastroDoPonto.setForeground(Color.WHITE);
		lblCadastroDoPonto.setFont(new Font("Dialog", Font.PLAIN, 21));
		lblCadastroDoPonto.setBounds(194, 30, 308, 26);
		cadastroPontoColeta.add(lblCadastroDoPonto);
		
		JButton btnCadastrarPontoColeta = new JButton("Cadastrar");
		btnCadastrarPontoColeta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String descricao = textDescricaoPontoColeta.getText();
				String endereco = textEnderecoPontoColeta.getText();
				String telefone = textTelefonePontoColeta.getText();
				CadastrarPontoColeta(descricao,endereco,telefone);
			}
		});
		btnCadastrarPontoColeta.setBounds(132, 242, 117, 29);
		cadastroPontoColeta.add(btnCadastrarPontoColeta);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/fundo 1.png")));
		lblNewLabel.setBounds(0, 0, 680, 368);
		cadastroPontoColeta.add(lblNewLabel);
		
		JPanel cadastroAdministrador = new JPanel();
		panel.add(cadastroAdministrador, "name_2899749703192");
		cadastroAdministrador.setLayout(null);
		
		JLabel label = new JLabel("Nome");
		label.setBounds(87, 105, 61, 16);
		cadastroAdministrador.add(label);
		
		JLabel label_1 = new JLabel("CPF");
		label_1.setBounds(87, 147, 61, 16);
		cadastroAdministrador.add(label_1);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(87, 188, 61, 16);
		cadastroAdministrador.add(lblEmail);
		
		JLabel label_3 = new JLabel("Data Nasc.");
		label_3.setBounds(87, 233, 80, 16);
		cadastroAdministrador.add(label_3);
		
		JLabel label_4 = new JLabel("Login");
		label_4.setBounds(87, 273, 61, 16);
		cadastroAdministrador.add(label_4);
		
		JLabel label_5 = new JLabel("Senha");
		label_5.setBounds(87, 312, 61, 16);
		cadastroAdministrador.add(label_5);
		
		textNomeAdministrador = new JTextField();
		textNomeAdministrador.setColumns(10);
		textNomeAdministrador.setBounds(160, 100, 416, 26);
		cadastroAdministrador.add(textNomeAdministrador);
		
		textCPFAdministrador = new JTextField();
		textCPFAdministrador.setColumns(10);
		textCPFAdministrador.setBounds(160, 147, 181, 26);
		cadastroAdministrador.add(textCPFAdministrador);
		
		textEmailAdministrador = new JTextField();
		textEmailAdministrador.setColumns(10);
		textEmailAdministrador.setBounds(160, 183, 416, 26);
		cadastroAdministrador.add(textEmailAdministrador);
		
		textDataNascAdministrador = new JTextField();
		textDataNascAdministrador.setColumns(10);
		textDataNascAdministrador.setBounds(160, 228, 181, 26);
		cadastroAdministrador.add(textDataNascAdministrador);
		
		textLoginAdministrador = new JTextField();
		textLoginAdministrador.setColumns(10);
		textLoginAdministrador.setBounds(160, 273, 181, 26);
		cadastroAdministrador.add(textLoginAdministrador);
		
		passwordAdministrador = new JPasswordField();
		passwordAdministrador.setBounds(160, 307, 181, 26);
		cadastroAdministrador.add(passwordAdministrador);
		
		JButton btnCadastrarAdministrador = new JButton("Cadastrar");
		btnCadastrarAdministrador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//BOTAO CADASTRAR ADMINISTRADOR
				TelaPrincipal tp = new TelaPrincipal();
				tp.CadastrarAdministrador();
			}
		});
		btnCadastrarAdministrador.setBounds(422, 268, 117, 29);
		cadastroAdministrador.add(btnCadastrarAdministrador);
		
		JLabel lblCadastroDeAdministrador = new JLabel("Cadastro de Administrador");
		lblCadastroDeAdministrador.setFont(new Font("Dialog", Font.PLAIN, 21));
		lblCadastroDeAdministrador.setBounds(215, 32, 290, 26);
		cadastroAdministrador.add(lblCadastroDeAdministrador);
		
		JPanel cadastroEditor = new JPanel();
		panel.add(cadastroEditor, "name_2987244874762");
		cadastroEditor.setLayout(null);
		
		JLabel label_7 = new JLabel("Nome");
		label_7.setBounds(97, 103, 61, 16);
		cadastroEditor.add(label_7);
		
		JLabel label_8 = new JLabel("CPF");
		label_8.setBounds(97, 145, 61, 16);
		cadastroEditor.add(label_8);
		
		JLabel lblEmail_1 = new JLabel("Email");
		lblEmail_1.setBounds(97, 186, 61, 16);
		cadastroEditor.add(lblEmail_1);
		
		JLabel label_10 = new JLabel("Data Nasc.");
		label_10.setBounds(97, 231, 80, 16);
		cadastroEditor.add(label_10);
		
		JLabel label_11 = new JLabel("Login");
		label_11.setBounds(97, 271, 61, 16);
		cadastroEditor.add(label_11);
		
		JLabel label_12 = new JLabel("Senha");
		label_12.setBounds(97, 310, 61, 16);
		cadastroEditor.add(label_12);
		
		textNomeEditor = new JTextField();
		textNomeEditor.setColumns(10);
		textNomeEditor.setBounds(170, 98, 416, 26);
		cadastroEditor.add(textNomeEditor);
		
		textCPFEditor = new JTextField();
		textCPFEditor.setColumns(10);
		textCPFEditor.setBounds(170, 145, 181, 26);
		cadastroEditor.add(textCPFEditor);
		
		textEmailEditor = new JTextField();
		textEmailEditor.setColumns(10);
		textEmailEditor.setBounds(170, 181, 416, 26);
		cadastroEditor.add(textEmailEditor);
		
		textDataNascEditor = new JTextField();
		textDataNascEditor.setColumns(10);
		textDataNascEditor.setBounds(170, 226, 181, 26);
		cadastroEditor.add(textDataNascEditor);
		
		textLoginEditor = new JTextField();
		textLoginEditor.setColumns(10);
		textLoginEditor.setBounds(170, 271, 181, 26);
		cadastroEditor.add(textLoginEditor);
		
		passwordEditor = new JPasswordField();
		passwordEditor.setBounds(170, 305, 181, 26);
		cadastroEditor.add(passwordEditor);
		
		JButton btnCadastrarEditor = new JButton("Cadastrar");
		btnCadastrarEditor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//BOTAO CADASTRAR EDITOR
				TelaPrincipal tp = new TelaPrincipal();
				tp.CadastrarEditor();
			}
		});
		btnCadastrarEditor.setBounds(432, 266, 117, 29);
		cadastroEditor.add(btnCadastrarEditor);
		
		JLabel lblCadastroDeEditor = new JLabel("Cadastro de Editor");
		lblCadastroDeEditor.setFont(new Font("Dialog", Font.PLAIN, 21));
		lblCadastroDeEditor.setBounds(241, 30, 236, 26);
		cadastroEditor.add(lblCadastroDeEditor);
		
		JPanel cadastroModerador = new JPanel();
		panel.add(cadastroModerador, "name_3013348392661");
		cadastroModerador.setLayout(null);
		
		JLabel label_14 = new JLabel("Nome");
		label_14.setBounds(93, 107, 61, 16);
		cadastroModerador.add(label_14);
		
		JLabel label_15 = new JLabel("CPF");
		label_15.setBounds(93, 149, 61, 16);
		cadastroModerador.add(label_15);
		
		JLabel lblEmail_2 = new JLabel("Email");
		lblEmail_2.setBounds(93, 190, 61, 16);
		cadastroModerador.add(lblEmail_2);
		
		JLabel label_17 = new JLabel("Data Nasc.");
		label_17.setBounds(93, 235, 80, 16);
		cadastroModerador.add(label_17);
		
		JLabel label_18 = new JLabel("Login");
		label_18.setBounds(93, 275, 61, 16);
		cadastroModerador.add(label_18);
		
		JLabel label_19 = new JLabel("Senha");
		label_19.setBounds(93, 314, 61, 16);
		cadastroModerador.add(label_19);
		
		textNomeModerador = new JTextField();
		textNomeModerador.setColumns(10);
		textNomeModerador.setBounds(166, 102, 416, 26);
		cadastroModerador.add(textNomeModerador);
		
		textCPFModerador = new JTextField();
		textCPFModerador.setColumns(10);
		textCPFModerador.setBounds(166, 149, 181, 26);
		cadastroModerador.add(textCPFModerador);
		
		textEmailModerador = new JTextField();
		textEmailModerador.setColumns(10);
		textEmailModerador.setBounds(166, 185, 416, 26);
		cadastroModerador.add(textEmailModerador);
		
		textDataNascModerador = new JTextField();
		textDataNascModerador.setColumns(10);
		textDataNascModerador.setBounds(166, 230, 181, 26);
		cadastroModerador.add(textDataNascModerador);
		
		textLoginModerador = new JTextField();
		textLoginModerador.setColumns(10);
		textLoginModerador.setBounds(166, 275, 181, 26);
		cadastroModerador.add(textLoginModerador);
		
		passwordModerador = new JPasswordField();
		passwordModerador.setBounds(166, 309, 181, 26);
		cadastroModerador.add(passwordModerador);
		
		JButton btnCadastrarModerador = new JButton("Cadastrar");
		btnCadastrarModerador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//BOTAO CADASTRAR MODERADOR
				TelaPrincipal tp = new TelaPrincipal();
				tp.CadastrarModerador();
			}
		});
		btnCadastrarModerador.setBounds(428, 270, 117, 29);
		cadastroModerador.add(btnCadastrarModerador);
		
		JLabel lblCadastroDeModerador = new JLabel("Cadastro de Moderador");
		lblCadastroDeModerador.setFont(new Font("Dialog", Font.PLAIN, 21));
		lblCadastroDeModerador.setBounds(237, 34, 236, 26);
		cadastroModerador.add(lblCadastroDeModerador);
		
		JPanel artigos = new JPanel();
		panel.add(artigos, "name_166140401146762");
		artigos.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(36, 0, 600, 368);
		artigos.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblAlbel = new JLabel("<html>\r\n<div style='text-align: center;'>\r\n<img src='classpath:/img/fundo1.png'></img>\r\n<br>\r\n<h1>T\u00EDtulo do Artigo</h1>\r\n<p>Corpo do artigo</p>\r\n</div>\r\n</html>");
		lblAlbel.setBounds(399, 21, 161, 141);
		panel_1.add(lblAlbel);
		
		JLabel label_40 = new JLabel("<html>\r\n<div style='text-align: center;'>\r\n<img src='classpath:/img/fundo1.png'></img>\r\n<br>\r\n<h1>T\u00EDtulo do Artigo</h1>\r\n<p>Corpo do artigo</p>\r\n</div>\r\n</html>");
		label_40.setBounds(213, 21, 161, 141);
		panel_1.add(label_40);
		
		JLabel lblttuloDoArtigo = new JLabel("<html>\r\n<div style='text-align: center;'>\r\n<img src='fundo1.png'></img>\r\n<br>\r\n<h1>T\u00EDtulo do Artigo</h1>\r\n<p>Corpo do artigo</p>\r\n</div>\r\n</html>");
		lblttuloDoArtigo.setBounds(31, 21, 161, 141);
		panel_1.add(lblttuloDoArtigo);
		
		JLabel label_41 = new JLabel("<html>\r\n<div style='text-align: center;'>\r\n<img src='classpath:/img/fundo1.png'></img>\r\n<br>\r\n<h1>T\u00EDtulo do Artigo</h1>\r\n<p>Corpo do artigo</p>\r\n</div>\r\n</html>");
		label_41.setBounds(217, 191, 161, 141);
		panel_1.add(label_41);
		
		JLabel label_42 = new JLabel("<html>\r\n<div style='text-align: center;'>\r\n<img src='classpath:/img/fundo1.png'></img>\r\n<br>\r\n<h1>T\u00EDtulo do Artigo</h1>\r\n<p>Corpo do artigo</p>\r\n</div>\r\n</html>");
		label_42.setBounds(399, 192, 161, 141);
		panel_1.add(label_42);
		
		JLabel label_43 = new JLabel("<html>\r\n<div style='text-align: center;'>\r\n<img src='classpath:/img/fundo1.png'></img>\r\n<br>\r\n<h1>T\u00EDtulo do Artigo</h1>\r\n<p>Corpo do artigo</p>\r\n</div>\r\n</html>");
		label_43.setBounds(36, 192, 161, 141);
		panel_1.add(label_43);
		
		JLabel label_9 = new JLabel("");
		label_9.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/fundo 1.png")));
		label_9.setBounds(0, 0, 680, 368);
		artigos.add(label_9);
		
		JPanel mapa = new JPanel();
		panel.add(mapa, "name_166196253626163");
		mapa.setLayout(null);
		
		JLabel label_13 = new JLabel("");
		label_13.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/MAPAS.png")));
		label_13.setBounds(0, 0, 680, 368);
		mapa.add(label_13);
		
		JPanel alterarUsuario = new JPanel();
		panel.add(alterarUsuario, "name_167612328579360");
		alterarUsuario.setLayout(null);
		
		JLabel lblAlteraoDeUsurio = new JLabel("Altera\u00E7\u00E3o de Usu\u00E1rio");
		lblAlteraoDeUsurio.setForeground(Color.WHITE);
		lblAlteraoDeUsurio.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblAlteraoDeUsurio.setBounds(164, 29, 349, 56);
		alterarUsuario.add(lblAlteraoDeUsurio);
		
		JButton btnDadosCadastrais = new JButton("Dados Cadastrais");
		btnDadosCadastrais.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardLayout card = (CardLayout)panel.getLayout();
	            card.show(panel,"name_4083723951147");
			}
		});
		btnDadosCadastrais.setBounds(239, 213, 168, 23);
		alterarUsuario.add(btnDadosCadastrais);
		
		JButton btnAlterarSenha = new JButton("Alterar Senha");
		btnAlterarSenha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CardLayout card = (CardLayout)panel.getLayout();
	            card.show(panel,"name_4255425467672");
			}
		});
		btnAlterarSenha.setBounds(239, 138, 168, 23);
		alterarUsuario.add(btnAlterarSenha);
		
		JLabel label_20 = new JLabel("");
		label_20.setBounds(0, 0, 688, 417);
		alterarUsuario.add(label_20);
		label_20.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/fundo crejoa.png")));
		
		JPanel alterarDados = new JPanel();
		panel.add(alterarDados, "name_4083723951147");
		alterarDados.setLayout(null);
		
		JLabel lblAlterarDadosCadastrais = new JLabel("Alterar Dados Cadastrais");
		lblAlterarDadosCadastrais.setForeground(Color.WHITE);
		lblAlterarDadosCadastrais.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblAlterarDadosCadastrais.setBounds(121, 24, 436, 56);
		alterarDados.add(lblAlterarDadosCadastrais);
		
		JLabel lblLogin_2 = new JLabel("Login:");
		lblLogin_2.setForeground(Color.WHITE);
		lblLogin_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblLogin_2.setBounds(35, 104, 99, 16);
		alterarDados.add(lblLogin_2);
		
		txtLoginAltDados = new JTextField();
		txtLoginAltDados.setColumns(10);
		txtLoginAltDados.setBounds(139, 104, 137, 20);
		alterarDados.add(txtLoginAltDados);
		
		txtSenhaAltDados = new JTextField();
		txtSenhaAltDados.setColumns(10);
		txtSenhaAltDados.setBounds(139, 131, 137, 20);
		alterarDados.add(txtSenhaAltDados);
		
		JLabel lblSenha_1 = new JLabel("Senha:");
		lblSenha_1.setForeground(Color.WHITE);
		lblSenha_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSenha_1.setBounds(35, 131, 113, 16);
		alterarDados.add(lblSenha_1);
		
		JLabel lblNome_1 = new JLabel("Nome:");
		lblNome_1.setForeground(Color.WHITE);
		lblNome_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNome_1.setBounds(35, 180, 99, 16);
		alterarDados.add(lblNome_1);
		
		txtNomeAlterar = new JTextField();
		txtNomeAlterar.setColumns(10);
		txtNomeAlterar.setBounds(139, 180, 275, 20);
		alterarDados.add(txtNomeAlterar);
		
		JButton button = new JButton("Alterar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TelaPrincipal tp = new TelaPrincipal();
				String login = txtLoginAltDados.getText();
				String senha = txtSenhaAltDados.getText();
				String nome = txtNomeAlterar.getText();
				String email = txtEmailAlterar.getText();
				tp.AlterarDadosUsuario(login, senha, nome, email);
			}
		});
		button.setBounds(352, 265, 117, 29);
		alterarDados.add(button);
		
		JLabel lblEmail_3 = new JLabel("Email:");
		lblEmail_3.setForeground(Color.WHITE);
		lblEmail_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEmail_3.setBounds(35, 207, 99, 16);
		alterarDados.add(lblEmail_3);
		
		txtEmailAlterar = new JTextField();
		txtEmailAlterar.setColumns(10);
		txtEmailAlterar.setBounds(139, 207, 275, 20);
		alterarDados.add(txtEmailAlterar);
		
		JLabel label_23 = new JLabel("");
		label_23.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/fundo 1.png")));
		label_23.setBounds(0, 0, 688, 417);
		alterarDados.add(label_23);
		
		JPanel alterarSenha = new JPanel();
		panel.add(alterarSenha, "name_4255425467672");
		alterarSenha.setLayout(null);
		
		JLabel lblLogin_1 = new JLabel("Login");
		lblLogin_1.setForeground(Color.WHITE);
		lblLogin_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblLogin_1.setBounds(114, 96, 99, 16);
		alterarSenha.add(lblLogin_1);
		
		JLabel lblSenhaAntiga = new JLabel("Senha antiga:");
		lblSenhaAntiga.setForeground(Color.WHITE);
		lblSenhaAntiga.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblSenhaAntiga.setBounds(110, 141, 113, 16);
		alterarSenha.add(lblSenhaAntiga);
		
		txtLoginAlterar = new JTextField();
		txtLoginAlterar.setBounds(233, 96, 137, 20);
		alterarSenha.add(txtLoginAlterar);
		txtLoginAlterar.setColumns(10);
		
		txtSenhaAntigaAlterar = new JTextField();
		txtSenhaAntigaAlterar.setBounds(233, 141, 137, 20);
		alterarSenha.add(txtSenhaAntigaAlterar);
		txtSenhaAntigaAlterar.setColumns(10);
		
		txtSenhaNovaAlterar = new JTextField();
		txtSenhaNovaAlterar.setBounds(233, 192, 137, 20);
		alterarSenha.add(txtSenhaNovaAlterar);
		txtSenhaNovaAlterar.setColumns(10);
		
		JLabel lblNovaSenha = new JLabel("Nova senha:");
		lblNovaSenha.setForeground(Color.WHITE);
		lblNovaSenha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNovaSenha.setBounds(114, 195, 99, 16);
		alterarSenha.add(lblNovaSenha);
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaPrincipal tp = new TelaPrincipal();
				String login = txtLoginAlterar.getText();
				String senha = txtSenhaAntigaAlterar.getText();
				String novasenha = txtSenhaNovaAlterar.getText();
				tp.AlterarSenhaUsuario(login,senha,novasenha);
			}
		});
		btnAlterar.setBounds(246, 244, 117, 29);
		alterarSenha.add(btnAlterar);
		
		JLabel label_22 = new JLabel("");
		label_22.setBounds(0, 0, 690, 379);
		alterarSenha.add(label_22);
		label_22.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/fundo 1.png")));
		
		JPanel excluirUsuario = new JPanel();
		panel.add(excluirUsuario, "name_167659012360567");
		excluirUsuario.setLayout(null);
		
		JLabel lblLoginDel = new JLabel("Login");
		lblLoginDel.setForeground(Color.WHITE);
		lblLoginDel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblLoginDel.setBounds(152, 134, 78, 22);
		excluirUsuario.add(lblLoginDel);
		
		JLabel lblSenhaDel = new JLabel("Senha");
		lblSenhaDel.setForeground(Color.WHITE);
		lblSenhaDel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblSenhaDel.setBounds(152, 176, 78, 22);
		excluirUsuario.add(lblSenhaDel);
		
		JButton btnDeletar = new JButton("Deletar");
		btnDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TelaPrincipal tp = new TelaPrincipal();
				String login = txtLoginDel.getText();
				String senha = txtSenhaDel.getText();
				boolean status = tp.DeletarUsuario(login,senha);
				if(status) {
					JOptionPane.showMessageDialog(null, "Usu�rio "+login+" deletado com sucesso!");
				}
			}
		});
		btnDeletar.setBounds(260, 238, 89, 23);
		excluirUsuario.add(btnDeletar);
		
		JLabel lblDeletarUsurio = new JLabel("Deletar Usu\u00E1rio");
		lblDeletarUsurio.setFont(new Font("Tahoma", Font.PLAIN, 23));
		lblDeletarUsurio.setForeground(Color.WHITE);
		lblDeletarUsurio.setBounds(227, 48, 175, 28);
		excluirUsuario.add(lblDeletarUsurio);
		
		txtLoginDel = new JTextField();
		txtLoginDel.setBounds(229, 135, 175, 20);
		excluirUsuario.add(txtLoginDel);
		txtLoginDel.setColumns(10);
		
		txtSenhaDel = new JPasswordField();
		txtSenhaDel.setBounds(229, 178, 175, 20);
		excluirUsuario.add(txtSenhaDel);
		
		JLabel label_21 = new JLabel("");
		label_21.setBounds(0, 0, 688, 417);
		label_21.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/fundo 1.png")));
		excluirUsuario.add(label_21);
		
		JPanel assossiacoes = new JPanel();
		panel.add(assossiacoes, "name_9977710703461");
		assossiacoes.setLayout(null);
		
		JLabel label_24 = new JLabel("Cooperativa A\u00E7\u00E3o Reciclar");
		label_24.setForeground(Color.WHITE);
		label_24.setFont(new Font("Arial", Font.BOLD, 14));
		label_24.setBounds(396, 80, 215, 14);
		assossiacoes.add(label_24);
		
		JLabel label_25 = new JLabel("R. 13 de Maio, 85 - Jd. America");
		label_25.setForeground(Color.WHITE);
		label_25.setFont(new Font("Arial", Font.PLAIN, 14));
		label_25.setBounds(394, 244, 231, 14);
		assossiacoes.add(label_25);
		
		JLabel label_26 = new JLabel("R. Zicornio, 600 - Jd. Kennedy");
		label_26.setForeground(Color.WHITE);
		label_26.setFont(new Font("Arial", Font.PLAIN, 14));
		label_26.setBounds(95, 124, 231, 14);
		assossiacoes.add(label_26);
		
		JLabel label_27 = new JLabel("R. Albertino Gomes da Silva, 251 ");
		label_27.setForeground(Color.WHITE);
		label_27.setFont(new Font("Arial", Font.PLAIN, 14));
		label_27.setBounds(95, 244, 351, 14);
		assossiacoes.add(label_27);
		
		JLabel label_28 = new JLabel("Av. Leonor Furlaneto Delgado, 239 ");
		label_28.setForeground(Color.WHITE);
		label_28.setFont(new Font("Arial", Font.PLAIN, 14));
		label_28.setBounds(394, 124, 317, 14);
		assossiacoes.add(label_28);
		
		JLabel label_29 = new JLabel("");
		label_29.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/icons8-pointer.png")));
		label_29.setBounds(46, 207, 58, 51);
		assossiacoes.add(label_29);
		
		JLabel label_30 = new JLabel("Jd. Philadelphia");
		label_30.setForeground(Color.WHITE);
		label_30.setFont(new Font("Arial", Font.PLAIN, 14));
		label_30.setBounds(396, 149, 231, 14);
		assossiacoes.add(label_30);
		
		JLabel label_31 = new JLabel("");
		label_31.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/icons8-pointer.png")));
		label_31.setBounds(46, 80, 58, 51);
		assossiacoes.add(label_31);
		
		JLabel label_32 = new JLabel("Jd. Sebasti\u00E3o");
		label_32.setForeground(Color.WHITE);
		label_32.setFont(new Font("Arial", Font.PLAIN, 14));
		label_32.setBounds(95, 269, 231, 14);
		assossiacoes.add(label_32);
		
		JLabel label_33 = new JLabel("");
		label_33.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/icons8-pointer.png")));
		label_33.setBounds(336, 207, 58, 51);
		assossiacoes.add(label_33);
		
		JLabel label_34 = new JLabel("");
		label_34.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/icons8-pointer.png")));
		label_34.setBounds(336, 87, 58, 51);
		assossiacoes.add(label_34);
		
		JLabel label_35 = new JLabel("Disk Ol\u00E9o Coopergore");
		label_35.setForeground(Color.WHITE);
		label_35.setFont(new Font("Arial", Font.BOLD, 14));
		label_35.setBounds(394, 207, 177, 14);
		assossiacoes.add(label_35);
		
		JLabel label_36 = new JLabel("Associa\u00E7\u00E3o Recriando");
		label_36.setForeground(Color.WHITE);
		label_36.setFont(new Font("Arial", Font.BOLD, 16));
		label_36.setBounds(95, 79, 195, 14);
		assossiacoes.add(label_36);
		
		JLabel label_37 = new JLabel("Coopersul Recicla Total");
		label_37.setForeground(Color.WHITE);
		label_37.setFont(new Font("Arial", Font.BOLD, 14));
		label_37.setBounds(100, 206, 215, 14);
		assossiacoes.add(label_37);
		
		JLabel label_38 = new JLabel("");
		label_38.setIcon(new ImageIcon(TelaPrincipal.class.getResource("/img/fundo 1.png")));
		label_38.setBounds(0, 0, 690, 379);
		assossiacoes.add(label_38);
		
		mnInicio.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				CardLayout card = (CardLayout)panel.getLayout();
	            card.show(panel,"name_166070114857929");
			}
		});
		
		mnArtigos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				CardLayout card = (CardLayout)panel.getLayout();
	            card.show(panel,"name_166140401146762");
			}
		});
		
		mnMapa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
//				CardLayout card = (CardLayout)panel.getLayout();
//	            card.show(panel,"name_166196253626163");
//	            
//				MapViewOptions m = new MapViewOptions();
//				Mapa mapa = new Mapa(m);
//				mapa.setVisible(true);
			}
		});
		
		mntmAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardLayout card = (CardLayout)panel.getLayout();
	            card.show(panel,"name_167612328579360");
			}
		});
		
		mntmExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardLayout card = (CardLayout)panel.getLayout();
	            card.show(panel,"name_167659012360567");
			}
		});
		JMenuItem mntmAssociaes = new JMenuItem("Associa\u00E7\u00F5es ");
		mntmAssociaes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//aqui vai pras associacoes
				CardLayout card = (CardLayout)panel.getLayout();
	            card.show(panel,"name_9977710703461");
			}
		});
		mnContatos.add(mntmAssociaes);
		
		JMenuItem mntmVerMapa = new JMenuItem("Ver Mapa");
		mntmVerMapa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardLayout card = (CardLayout)panel.getLayout();
	            card.show(panel,"name_166196253626163");
	            
				MapViewOptions m = new MapViewOptions();
				Mapa mapa = new Mapa(m);
				mapa.setVisible(true);
			}
		});
		mnMapa.add(mntmVerMapa);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Cadastrar Ponto");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//aqui vai para o cadastro de ponto
				CardLayout card = (CardLayout)panel.getLayout();
	            card.show(panel,"name_840788379422");
			}
		});
		mnMapa.add(mntmNewMenuItem);
		
	} //aqui acaba o jframe
	//aqui comeca os outros metodos

	//METODOS CERTOS (RECEBENDO OS DADOS JA NOS PARAMETROS)
	public void CadastrarUsuario(String nome, String email, String login, String senha) {
		UsuarioCtrl uc = new UsuarioCtrl();
		uc.Cadastrar(nome, email, login, senha);
	}
	public void AlterarSenhaUsuario (String login, String senha, String novasenha){
		UsuarioCtrl uc = new UsuarioCtrl();
		uc.AlterarSenha(login, senha, novasenha);
	}
	public void AlterarDadosUsuario (String login, String senha, String nome, String email){
		UsuarioCtrl uc = new UsuarioCtrl();
		uc.AlterarDados(login, senha, nome, email);
	}
	public void CadastrarPontoColeta(String descricao,String endereco, String telefone) {
		PontoColetaCtrl pcc = new PontoColetaCtrl();
		pcc.performGeocode(endereco, telefone, descricao);
	}
	public boolean DeletarUsuario(String login, String senha) {
		UsuarioCtrl uc = new UsuarioCtrl();
		boolean status = false;
		int autenticacao = uc.Login(login, senha);
		if(autenticacao==1) {
			status = uc.DeletarUsuario(login);
		}
		return status;
	}
	// METODOS ERRADOS (NAO CONSEGUE PUXAR O "TEXTBLABLA.GETTEXT()")
	public void CadastrarEditor() {
		EditorCtrl ec = new EditorCtrl();
		String nome = textNomeEditor.getText();
		String cpf = textCPFEditor.getText();
		String email = textEmailEditor.getText();
		String login = textLoginEditor.getText();
		String senha = passwordEditor.getText();
		String dataNasc = textDataNascEditor.getText();
		ec.Cadastrar(nome, email, login, senha);
	}
	public void CadastrarModerador() {
		ModeradorCtrl mc = new ModeradorCtrl();
		String nome = textNomeModerador.getText();
		String email = textEmailModerador.getText();
		String login = textLoginModerador.getText();
		String senha = passwordModerador.getText();
		mc.Cadastrar(nome, email, login, senha);

	}
	
	public void CadastrarAdministrador() {
		AdministradorCtrl ac = new AdministradorCtrl();
		String nome = textNomeAdministrador.getText();
		String email = textEmailAdministrador.getText();
		String login = textLoginAdministrador.getText();
		String senha = passwordAdministrador.getText();
		ac.Cadastrar(nome, email, login, senha);
	}
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}
