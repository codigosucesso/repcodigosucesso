package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.*;

import dao.*;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.CardLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import javax.swing.JFormattedTextField;
import javax.swing.DropMode;
import javax.swing.AbstractAction;
import javax.swing.Action;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import java.awt.Color;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtEmail;
	private JTextField txtLogin;
	private JPasswordField passwordSenha;
	private JTextField txtLoginLogin;
	private JPasswordField txtLoginSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		setTitle("Crejo\u00E1 | Login");
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 690, 423);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 690, 379);
		contentPane.add(panel);
		panel.setLayout(new CardLayout(0, 0));
		
		JPanel inicio = new JPanel();
		panel.add(inicio, "name_166070114857929");
		inicio.setLayout(null);
		
		JButton btnCriarConta = new JButton("Criar conta");
		btnCriarConta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardLayout card = (CardLayout)panel.getLayout();
	            card.show(panel,"name_15757764743602");
			}
		});
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Login.class.getResource("/img/crejoa tipo br130px.png")));
		label.setBounds(240, 11, 212, 149);
		inicio.add(label);
		btnCriarConta.setBounds(269, 330, 154, 23);
		inicio.add(btnCriarConta);
		
		JLabel lblNoPossuiUma = new JLabel("N\u00E3o possui uma conta? Clique abaixo para criar uma.");
		lblNoPossuiUma.setForeground(Color.WHITE);
		lblNoPossuiUma.setBounds(186, 305, 369, 14);
		inicio.add(lblNoPossuiUma);
		
		JLabel lblNome_1 = new JLabel("Login:");
		lblNome_1.setForeground(Color.WHITE);
		lblNome_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNome_1.setBounds(196, 172, 46, 14);
		inicio.add(lblNome_1);
		
		txtLoginLogin = new JTextField();
		txtLoginLogin.setBounds(261, 171, 173, 20);
		inicio.add(txtLoginLogin);
		txtLoginLogin.setColumns(10);
		
		JLabel lblSenha_1 = new JLabel("Senha:");
		lblSenha_1.setForeground(Color.WHITE);
		lblSenha_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSenha_1.setBounds(196, 213, 73, 14);
		inicio.add(lblSenha_1);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Login l = new Login();
				String login = txtLoginLogin.getText();
				String senha = txtLoginSenha.getText();
				int log = l.Login(login, senha);
				if(log==1){
					TelaPrincipal tp = new TelaPrincipal();
					tp.setVisible(true);
					l.setVisible(false);
					l.dispose();
				}else if(log==2){
					JOptionPane.showMessageDialog(null, "Usuario ou senha incorretos!");
				}else if(log==0){
					JOptionPane.showMessageDialog(null, "Login n�o existe ou campo n�o preenchido.");
				}
			}
		});
		btnEntrar.setBounds(303, 256, 89, 23);
		inicio.add(btnEntrar);
		
		txtLoginSenha = new JPasswordField();
		txtLoginSenha.setBounds(261, 212, 173, 20);
		inicio.add(txtLoginSenha);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon(Login.class.getResource("/img/fundo crejoa.png")));
		label_1.setBounds(0, 0, 680, 394);
		inicio.add(label_1);
		
		JPanel cadastroUsuario = new JPanel();
		panel.add(cadastroUsuario, "name_15757764743602");
		cadastroUsuario.setLayout(null);
		
		JLabel lblCadastroDeUsuario = new JLabel("Cadastro de Usuarios");
		lblCadastroDeUsuario.setForeground(Color.WHITE);
		lblCadastroDeUsuario.setFont(new Font("Lucida Grande", Font.PLAIN, 21));
		lblCadastroDeUsuario.setBounds(214, 31, 236, 26);
		cadastroUsuario.add(lblCadastroDeUsuario);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNome.setForeground(Color.WHITE);
		lblNome.setBounds(56, 109, 61, 16);
		cadastroUsuario.add(lblNome);
		
		JLabel lblEndereco = new JLabel("Email");
		lblEndereco.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblEndereco.setForeground(Color.WHITE);
		lblEndereco.setBounds(56, 192, 61, 16);
		cadastroUsuario.add(lblEndereco);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblLogin.setForeground(Color.WHITE);
		lblLogin.setBounds(56, 277, 61, 16);
		cadastroUsuario.add(lblLogin);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSenha.setForeground(Color.WHITE);
		lblSenha.setBounds(56, 316, 61, 16);
		cadastroUsuario.add(lblSenha);
		
		txtNome = new JTextField();
		txtNome.setBounds(143, 99, 416, 26);
		cadastroUsuario.add(txtNome);
		txtNome.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(143, 182, 416, 26);
		cadastroUsuario.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtLogin = new JTextField();
		txtLogin.setBounds(143, 272, 181, 26);
		cadastroUsuario.add(txtLogin);
		txtLogin.setColumns(10);
		
		JButton btnCadastrarUsuario = new JButton("Cadastrar");
		btnCadastrarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//BOTAO CADASTRAR USUARIO
				Login tp = new Login();
				String nome = txtNome.getText();
				String email = txtEmail.getText();
				String login = txtLogin.getText();
				String senha = passwordSenha.getText();
				tp.CadastrarUsuario(nome,email,login,senha);
			}
		});
		btnCadastrarUsuario.setBounds(405, 267, 117, 29);
		cadastroUsuario.add(btnCadastrarUsuario);
		
		passwordSenha = new JPasswordField();
		passwordSenha.setBounds(143, 306, 181, 26);
		cadastroUsuario.add(passwordSenha);
		
		JButton btnIncio = new JButton("In\u00EDcio");
		btnIncio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					CardLayout card = (CardLayout)panel.getLayout();
		            card.show(panel,"name_166070114857929");
			}
		});
		btnIncio.setBounds(10, 11, 108, 23);
		cadastroUsuario.add(btnIncio);
		
		JLabel label_2 = new JLabel("");
		label_2.setIcon(new ImageIcon(Login.class.getResource("/img/fundo 1.png")));
		label_2.setBounds(0, 0, 680, 392);
		cadastroUsuario.add(label_2);
	} //aqui acaba o jframe
	//aqui come�a os outros metodos
	public void CadastrarUsuario(String nome, String email, String login, String senha) {
		UsuarioCtrl uc = new UsuarioCtrl();
		uc.Cadastrar(nome, email, login, senha);

	}
	public int Login(String login, String senha){
		UsuarioCtrl uc = new UsuarioCtrl();
		int log = uc.Login(login, senha);
		return log;
	}
	
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}
