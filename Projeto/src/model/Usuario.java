package model;

public class Usuario {

   public String nome;
   public String email;
   public String login;
   public String senha;
 
   public Usuario(String nome, String login, String email, String senha) {
	   this.email=email;
	   this.login=login;
	   this.nome=nome;
	   this.senha=senha;
   }
   
   public Usuario() {
	   
   }
   
   public String getNome() {
       return nome;
   }

   public void setNome(String nome) {
       this.nome = nome;
   }

   public String getEmail() {
       return email;
   }

   public void setEmail(String email) {
       this.email = email;
   }

   public String getLogin() {
       return login;
   }

   public void setLogin(String login) {
       this.login = login;
   }

   public String getSenha() {
       return senha;
   }

   public void setSenha(String senha) {
       this.senha = senha;
   }
}
