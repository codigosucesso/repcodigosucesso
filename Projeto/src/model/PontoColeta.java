package model;

import com.teamdev.jxmaps.LatLng;

public class PontoColeta {
	private int idPonto;
	private String descricao;
	private String endereco;
	private LatLng coordenadas;
	private String telefone;
	
	public PontoColeta(){
	}
	public int getIdPonto() {
		return idPonto;
	}
	public void setIdPonto(int idPonto) {
		this.idPonto = idPonto;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public LatLng getCoordenadas() {
		return coordenadas;
	}
	public void setCoordenadas(LatLng coordenadas) {
		this.coordenadas = coordenadas;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}	
}
