package dao;
import model.Usuario;

import static org.junit.Assert.*;

import org.junit.Test;
public class UsuarioDAOTest {
	
	@Test
	public void InserirTest() {
		Usuario p = new Usuario();
		UsuarioDAO d = new UsuarioDAO();
		String nome="a";
		String email="aa";
		String login="aaa";
		String senha="aa";
	
		
		p.setNome(nome);
		p.setEmail(email);
		p.setLogin(login);
		p.setSenha(senha);
		assertTrue(d.Inserir(p));
	}
	
	@Test
	public void DeletaUsuarioTest() {
		UsuarioDAO p = new UsuarioDAO();
		String login="aaa";
		assertTrue(p.DeletaUsuario(login));
	}
}