package controller;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dao.UsuarioDAO;
import model.Usuario;

public class UsuarioCtrlTest {
	
	private Usuario us=new Usuario("ps","ps","ps@email.com","senha");
	private UsuarioCtrl u;
	private UsuarioDAO usu;
	private String novasenha="aaa";
	
	@Before
	public void iniciar() {
		u=new UsuarioCtrl();
		
	}
	
	@Test
	public void CadastrarTest() {
		boolean status;
		status=u.Cadastrar(us.getNome(), us.getEmail(), us.getLogin(), us.getSenha());
		assertEquals(true, status);
	}

	
	//Testes Login
	@Test
	public void LoginSucessoTeste(){
		u.Cadastrar(us.getNome(), us.getEmail(), us.getLogin(), us.getSenha());
		assertEquals(1, u.Login(us.getLogin(), us.getSenha()));
	}
	
	@Test
	public void LoginIncorretoTeste(){
		u.Cadastrar(us.getNome(), us.getEmail(), us.getLogin(), us.getSenha());
		assertEquals(2, u.Login(us.getLogin(), "gg"));
	}
	
	@Test
	public void LoginNãoExisteTeste(){
		assertEquals(0, u.Login(us.getLogin(), "gg"));
	}
	
	
	//Testes Alterar Senha
	@Test
	public void AlterarSenhaNaoExisteTest() {
		assertEquals(0,u.AlterarSenha(us.getLogin(),us.getSenha(), novasenha));
	}
	
	@Test
	public void AlterarSenhaIncorretoTest() {
		u.Cadastrar(us.getNome(), us.getEmail(),us.getLogin(), us.getSenha());
		assertEquals(2,u.AlterarSenha(us.getLogin(),"ggg", novasenha));
	}
	
	@Test
	public void AlterarSenhaSucessoTest() {
		u.Cadastrar(us.getNome(), us.getEmail(),us.getLogin(), us.getSenha());
		assertEquals(1,u.AlterarSenha(us.getLogin(),us.getSenha(), novasenha));
	}
	
	
	//Testes Alterar Dados	
	@Test
	public void AlterarDadosNaoExisteTest() {
		assertEquals(0,u.AlterarDados(us.getLogin(),us.getSenha(), "OLA", "OLA@OLA.com"));
	}
	
	@Test
	public void AlterarDadosIncorretoTest() {
		u.Cadastrar(us.getNome(), us.getEmail(), us.getLogin(), us.getSenha());
		assertEquals(2,u.AlterarDados(us.getLogin(),"ggg", "OLA", "OLA@OLA.com"));
	}
	
	@Test
	public void AlterarDadosSucessoTest() {
		u.Cadastrar(us.getNome(), us.getEmail(), us.getLogin(), us.getSenha());
		assertEquals(1,u.AlterarDados(us.getLogin(),us.getSenha(), "OLA", "OLA@OLA.com"));
	}
	
	@Test
	public void TestePesquisar(){
		Usuario status;
		u.Cadastrar(us.getNome(), us.getEmail(), us.getLogin(), us.getSenha());
		status = u.Pesquisar(us.getEmail());
		assertEquals("ps@email.com", status.getEmail());
	}
	
	@After
	public void finalizar() {
		usu=new UsuarioDAO();
		usu.DeletaUsuario(us.getLogin());
	}
	
	

}