package controller;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dao.AdministradorDAO;
import model.Administrador;

public class AdministradorCtrlTest {
	
	private Administrador us=new Administrador("ps","ps","ps@email.com","senha");
	private AdministradorCtrl u;
	private AdministradorDAO usu;
	
	@Before
	public void iniciar() {
		u=new AdministradorCtrl();
	}
	
	@Test
	public void CadastrarTest() {
		assertEquals(true,u.Cadastrar(us.getNome(), us.getEmail(), us.getLogin(), us.getSenha()));
	}
	
	@After
	public void finalizar() {
		usu=new AdministradorDAO();
		usu.DeletaAdministrador(us.getLogin());
	}
	
	
	
}

