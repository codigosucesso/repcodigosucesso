package model;

import static org.junit.Assert.*;

import org.junit.Test;

public class ArtigoTest {

	@Test
	public void getIdArtigo() {
		Artigo a= new Artigo();
		a.setIdArtigo(1);
		assertEquals(1,a.getIdArtigo());
		
	}
	@Test
	public void getDataPublicacao() {
		Artigo a= new Artigo();
		a.setDataPublicacao("1 de janeiro de 2017");
		assertEquals("1 de janeiro de 2017",a.getDataPublicacao());
	}
	@Test
	public void getDataAprovacao() {
		Artigo a= new Artigo();
		a.setDataAprovacao("2 de janeiro de 2017 ");
		assertEquals("2 de janeiro de 2017",a.getDataAprovacao());
	}
	@Test
	public void getTitulo() {
		Artigo a= new Artigo();
		a.setTitulo("Titulo");
		assertEquals("Titulo",a.getTitulo());
	}
}
