package model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
public class UsuarioTest {
	
	private Usuario u;
	
	@Before 
	public void iniciar() {
		u = new Usuario();
	}
	
	@Test
	public void getNome() {
		u.setNome("Alguem");
		assertEquals("Alguem",u.getNome());
	}
	
	@Test
	public void getEmail() {
		u.setEmail("alguem@gmail.com");
		assertEquals("alguem@gmail.com",u.getEmail());
	
	}
	
	@Test
	public void getLogin() {
		u.setLogin("@alguem");
		assertEquals("@alguem",u.getLogin());
	}
	
	@Test
	public void getSenha() {
		u.setSenha("123");
		assertEquals("123",u.getSenha());
	}
}
